require 'platform-api'
require 'dotenv'
Dotenv.load

module Deploy
  extend self

  MissingHerokuToken = Class.new(StandardError)
  TokenName = 'HEROKU_TOKEN'

  def self.heroku_token
    ENV.fetch(TokenName) do |name|
      raise MissingHerokuToken,
      "The environment variable #{name} has not been set." \
      " The method #deploy_heroku cannot be run without it."
    end
  end

  def heroku_client
    PlatformAPI.connect_oauth(heroku_token)
  end

  def deploy_heroku(app_name)
    heroku_client.config_var.update(app_name, config_vars)
  end

  def config_vars
    Dotenv.load.delete_if{|k| k.eql? TokenName}
  end
end
