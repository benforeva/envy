require 'dotenv'
require_relative 'envy/deploy.rb'

# Module for loading environment variables and failing programs when variables
# are not set.
module Envy
  include Dotenv, Deploy

  EnvMissing = Class.new(StandardError)

  # Creates environment variables for `.env' and overwrites them with variables
  # present in `.env.test`
  def self.test_envs
    Dotenv.overload('.env.test')
  end

  # Returns the value of the specified environment variable or raises an
  # EnvMissing exception
  # @param env_name [String] name of the environment variable, which should match an
  #   entry in .env* files
  # @raise [EnvMissing] when the environment variable does not exist
  # @return [String, nil] value of envrionment variable when env_name is a valid
  #   name and nil otherwise
  def self.load_env(env_name)
    if env_name?(env_name)
      ENV.fetch(env_name){|name| raise EnvMissing, load_env_error_message(name)}
    end
  end

  # Entity check for EnvName parameter
  # @param data [Object] object under test
  # @return [Boolean] true if data is a valid EnvName, false otherwise
  def self.env_name?(data)
    data.is_a?(String) &&
      (data == data.upcase)
  end

  # Exception message for EnvMissing in #load_env
  # @param env_name [String] name of the environment variable, which should match an
  #   entry in .env* files
  # @return [String] error message
  def self.load_env_error_message(env_name)
    "#{env_name} is missing from the environment. "\
    "Please ensure that the entry #{env_name}=value-of-var is present in " \
    "your .env or .env.* file."
  end

  def self.deploy_heroku(app_name)
    Deploy.deploy_heroku(app_name)
  end
end
