require_relative 'lib/envy/version.rb'

Gem::Specification.new do |s|
  s.name        = 'envy.benforeva'
  s.version     = Envy::VERSION
  s.date        = '2018-07-24'
  s.summary     = 'strict environment management'
  s.description = "Envy fails programs dependent on missing" \
                  "environment variables"
  s.homepage    = "https://bitbucket.org/benforeva/envy/src/master/"
  s.license     = "MIT"
  s.authors     = ['Andre Dickosn']
  s.email       = 'andrebcdickson@gmail.com'
  s.files       = Dir['lib/envy.rb', 'lib/envy/*']
  s.add_runtime_dependency 'dotenv'
  s.add_runtime_dependency 'platform-api'
end
