# Envy

Envy is an environment manager that extends the [dotenv gem](https://github.com/bkeepers/dotenv) to fail programs when required environment variables are missing.

# System requirements
Tested on Ruby 2.4.4 only

## Installation

Install the gem:

```shell
gem install envy.benforeva
```

To use in your code:

```shell
require 'envy'
```

To add to your Gemfile:

```shell
gem 'envy.benforeva', require: 'envy'
```


## Usage

Require the gem and use the `Dotenv.load` command at the top of the file to load `.env` files. Set a constant to the value of an environment variable in your `.env` file using the `load_env` method.

```ruby
require 'envy'
Dotenv.load

class EnvyDemo
  PRIVATE_KEY = Envy.load_env('PRIVATE_KEY')
end
```

If the `PRIVATE_KEY` environment variable has been set then the constant will be assigned its value. Otherwise an `EnvMissing` exception will be raised when this file is either run or required. A sample error message is given below.

```ruby
"PRIVATE_KEY is missing from the environment.\n
Please ensure that the entry PRIVATE_KEY=value-of-env-var is present in\
your .env file or non-production variants. (EnvMissing)"
```

If you want to be more explicit about your dependencies use the `envs` method to list all the programs env variables at the top of the Module. This will raise an `EnvsMissing` exception if the environment variables passed have not been set.

```ruby
require 'envy'
Dotenv.load

class EnvyDemo
  Envy.envs 'PRIVATE_KEY', 'API_KEY'
end
```
