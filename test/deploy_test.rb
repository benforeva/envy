require 'minitest/autorun'
require 'minitest/pride'
require 'pp'
require_relative '../lib/envy/deploy.rb'

class TestDeploy < MiniTest::Test

  APP_NAME = 'test-app-envy-deploy'
  @@heroku = Deploy.heroku_client

  def all_apps
    @@heroku.app.list.map{|i| i["name"]}
  end

  def setup_temp_heroku_app
    @@heroku.app.create({'name' => APP_NAME}) unless all_apps.include?(APP_NAME)
  end

  MiniTest.after_run do
    app_names = @@heroku.app.list.map{|i| i["name"]}
    @@heroku.app.delete(APP_NAME) if app_names.include?(APP_NAME)
  end

  def test_deploy_heroku_requires_token_env_var
    ENV[Deploy::TokenName] = nil
    assert_raises(Deploy::MissingHerokuToken) {Deploy.heroku_token}
    Dotenv.load
  end

  def test_deploy_heroku_updates_config_vars
    setup_temp_heroku_app
    assert_equal Deploy.deploy_heroku(APP_NAME), Deploy.config_vars
  end

  def test_deploy_heroku_does_not_update_heroku_token_config_var
    setup_temp_heroku_app
    refute_includes Deploy.deploy_heroku(APP_NAME).keys, Deploy::TokenName
  end
end
