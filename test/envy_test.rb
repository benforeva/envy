require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/envy.rb'

class TestEnvy < MiniTest::Test

  def setup
    @set_value = "32984"
    ENV['SET'] = @set_value
  end

  def test_load_env_raises_exception_when_env_variable_is_not_set
    assert_raises(Envy::EnvMissing) {Envy.load_env('UNSET')}
  end

  def test_load_env_succeeds_when_env_variable_is_set
    assert_equal @set_value, Envy.load_env('SET')
  end

  def test_load_env_fails_when_name_is_not_a_string
    assert_nil Envy.load_env(32)
  end

  def test_load_env_fails_when_name_is_not_a_capitalized_string
    assert_nil Envy.load_env('set')
  end

  def test_test_env_loads_dotenv_environment_variables
    Envy.test_envs
    assert Envy.load_env('HEROKU_TOKEN')
  end

  def test_test_env_loads_dotenv_test_environment_variables
    Envy.test_envs
    assert Envy.load_env('ENV1')
    assert Envy.load_env('ENV2')
    assert Envy.load_env('ENV3')
  end

  def test_test_env_overwrites_conflicting_environment_variables
    Envy.test_envs
    assert_equal Envy.load_env('ENV1'), 'TEST_BAT'
    assert_equal Envy.load_env('ENV2'), 'TEST_BALL'
  end
end
