require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/envy.rb'
require_relative '../lib/envy/version.rb'

class TestGem < MiniTest::Test

  def test_version
    assert_equal '1.1.0', Envy::VERSION
  end

  def test_makes_dotenv_available
    assert Dotenv
  end

  def test_api
    assert_respond_to Envy, :load_env
    assert_respond_to Envy, :test_envs
    assert_respond_to Envy, :deploy_heroku
  end
end
